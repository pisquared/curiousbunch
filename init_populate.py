from oshipka.persistance import populate_static, init_db

from populate import populate_db
from webapp.app import app

init_db(app)
populate_static(app)
populate_db(app)
