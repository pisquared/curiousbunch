from config import TEMPLATES_FOLDER, STATIC_FOLDER
from oshipka.persistance import populate_static, init_db

from populate import populate_db
from webapp.app import app

if init_db(app):
    populate_static(app)
    populate_db(app)

app.template_folder = TEMPLATES_FOLDER
app.static_folder = STATIC_FOLDER

if __name__ == "__main__":
    app.run(debug=True)
