from flask import render_template, request

from webapp.routes import *
from oshipka.webapp import app


@app.route('/')
def home():
    per_page = request.args.get('per_page', 5)
    episodes_pagination = Episode.query.order_by(Episode.date.desc()).paginate(per_page=per_page, error_out=False)
    return render_template("home.html", episodes_pagination=episodes_pagination)


@app.route('/duska')
@app.route('/board')
def cb_board():
    timer = Timer.query.filter_by(id=1).first()
    return render_template("board.html", timer=timer)
