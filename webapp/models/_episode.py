from oshipka.persistance import db, ModelController, index_service, LiberalBoolean


class Episode(db.Model, ModelController):
    episode_short = db.Column(db.UnicodeText,)
    title = db.Column(db.UnicodeText,)
    date = db.Column(db.UnicodeText,)
    duration = db.Column(db.UnicodeText,)
    link = db.Column(db.UnicodeText,)
    participants = db.Column(db.UnicodeText,)
